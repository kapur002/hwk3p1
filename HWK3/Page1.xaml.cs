﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HWK3
{
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        //event handler for next Button
        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Page2("Welcome to page 2"));
        }
    }
}
