﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HWK3
{
    public partial class Page2 : ContentPage
    {
        public Page2(string displayText)
        {
            InitializeComponent();
            data.Text = displayText;
        }
    }
}
